﻿namespace Hangman
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reset_button = new System.Windows.Forms.Button();
            this.unos_textBox = new System.Windows.Forms.TextBox();
            this.livesNo_label = new System.Windows.Forms.Label();
            this.livesleft_label = new System.Windows.Forms.Label();
            this.check_button = new System.Windows.Forms.Button();
            this.exit_button = new System.Windows.Forms.Button();
            this.main_label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // reset_button
            // 
            this.reset_button.Location = new System.Drawing.Point(497, 372);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(132, 66);
            this.reset_button.TabIndex = 0;
            this.reset_button.Text = "PLAY!";
            this.reset_button.UseVisualStyleBackColor = true;
            this.reset_button.Click += new System.EventHandler(this.reset_button_Click);
            // 
            // unos_textBox
            // 
            this.unos_textBox.Location = new System.Drawing.Point(347, 60);
            this.unos_textBox.Name = "unos_textBox";
            this.unos_textBox.Size = new System.Drawing.Size(68, 22);
            this.unos_textBox.TabIndex = 1;
            // 
            // livesNo_label
            // 
            this.livesNo_label.AutoSize = true;
            this.livesNo_label.Location = new System.Drawing.Point(732, 33);
            this.livesNo_label.Name = "livesNo_label";
            this.livesNo_label.Size = new System.Drawing.Size(56, 17);
            this.livesNo_label.TabIndex = 2;
            this.livesNo_label.Text = "number";
            // 
            // livesleft_label
            // 
            this.livesleft_label.AutoSize = true;
            this.livesleft_label.Location = new System.Drawing.Point(658, 33);
            this.livesleft_label.Name = "livesleft_label";
            this.livesleft_label.Size = new System.Drawing.Size(68, 17);
            this.livesleft_label.TabIndex = 3;
            this.livesleft_label.Text = "Lives left:";
            // 
            // check_button
            // 
            this.check_button.Location = new System.Drawing.Point(421, 60);
            this.check_button.Name = "check_button";
            this.check_button.Size = new System.Drawing.Size(75, 23);
            this.check_button.TabIndex = 4;
            this.check_button.Text = "CHECK";
            this.check_button.UseVisualStyleBackColor = true;
            this.check_button.Click += new System.EventHandler(this.check_button_Click);
            // 
            // exit_button
            // 
            this.exit_button.Location = new System.Drawing.Point(656, 372);
            this.exit_button.Name = "exit_button";
            this.exit_button.Size = new System.Drawing.Size(132, 66);
            this.exit_button.TabIndex = 5;
            this.exit_button.Text = "EXIT";
            this.exit_button.UseVisualStyleBackColor = true;
            this.exit_button.Click += new System.EventHandler(this.exit_button_Click);
            // 
            // main_label
            // 
            this.main_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.main_label.Location = new System.Drawing.Point(127, 160);
            this.main_label.Name = "main_label";
            this.main_label.Size = new System.Drawing.Size(599, 122);
            this.main_label.TabIndex = 6;
            this.main_label.Text = "Press PLAY so you could play HANGMAN!";
            this.main_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.main_label);
            this.Controls.Add(this.exit_button);
            this.Controls.Add(this.check_button);
            this.Controls.Add(this.livesleft_label);
            this.Controls.Add(this.livesNo_label);
            this.Controls.Add(this.unos_textBox);
            this.Controls.Add(this.reset_button);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button reset_button;
        private System.Windows.Forms.TextBox unos_textBox;
        private System.Windows.Forms.Label livesNo_label;
        private System.Windows.Forms.Label livesleft_label;
        private System.Windows.Forms.Button check_button;
        private System.Windows.Forms.Button exit_button;
        private System.Windows.Forms.Label main_label;
    }
}

