﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Hangman
{
    public partial class Form1 : Form
    {
        string secretWord;
        List<string> listaRijeci = new List<string>();
        int livesNo = 5;
        string mainWord;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) //START
        {
            string red;
            StreamReader reader;
            check_button.Visible = false;

            reader = new StreamReader(@"C:/Users/Robert/Desktop/rijeci.txt");
            while ((red = reader.ReadLine()) != null) //streamreader cita red po red
            {
                listaRijeci.Add(red); //svaki red(jednu rijec) dodajemo u listu
            }
        }

        private void reset_button_Click(object sender, EventArgs e)
        {
            Random rand = new Random();
            secretWord = listaRijeci[rand.Next(0, listaRijeci.Count - 1)]; //generiranje rijeci iz liste
            check_button.Visible = true; //pojavi se check button

            mainWord = new string('*', secretWord.Length);
            main_label.Text = mainWord;

            livesNo_label.Text = livesNo.ToString();
        }

        private void exit_button_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void check_button_Click(object sender, EventArgs e)
        {
            if (unos_textBox.Text.Length == 1)
            {
                if (secretWord.Contains(unos_textBox.Text))
                {

                    string temp = secretWord;
                    while (temp.Contains(unos_textBox.Text))
                    {
                        int index = temp.IndexOf(unos_textBox.Text);
                        StringBuilder builder = new StringBuilder(temp);

                        builder[index] = '*';
                        temp = builder.ToString();
                        StringBuilder builder2 = new StringBuilder(mainWord);

                        builder2[index] = Convert.ToChar(unos_textBox.Text);
                        mainWord = builder2.ToString();
                    }
                    
                    if (mainWord == secretWord)
                    {
                        main_label.Text = secretWord;
                        MessageBox.Show("YOU WIN! ");
                        unos_textBox.Clear();
                        check_button.Visible = false;
                        main_label.Text = "Press PLAY so you could play HANGMAN!";
                    }
                    unos_textBox.Clear();
                }
                else
                {
                    livesNo--; //manji broj zivota za jedan
                    livesNo_label.Text = livesNo.ToString();//azuriranje zivota
                    unos_textBox.Clear(); //brisanje unosa
                    if (livesNo == 0)
                    {
                        MessageBox.Show("GAME OVER");
                        check_button.Visible = false;
                        unos_textBox.Clear();
                        main_label.Text = "Press PLAY so you could play HANGMAN!";
                    }
                }
                
            }
            else
            {
                MessageBox.Show("You entered more than 1 letter, try again!");
                unos_textBox.Clear();
            }
        }
    }
}
